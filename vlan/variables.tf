variable "eqx_metal_client_id"             {}
variable "eqx_metal_client_secret"         {}
variable "eqx_metal_project_id"            {}
variable "eqx_metal_metro_code"            {}
variable "eqx_metal_vlan"                  {}
variable "eqx_metal_vlan_description"      {}
